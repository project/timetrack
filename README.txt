Currently we are not providing the schema with config.
Please create database table manually.

CREATE TABLE `timetrack_projects` (
 `pid` int(11) NOT NULL AUTO_INCREMENT,
 `project_number` varchar(255) NOT NULL,
 `project_name` varchar(255) NOT NULL,
 `department` varchar(255) NOT NULL,
 `project_type` varchar(255) NOT NULL,
 `restricted` varchar(255) NOT NULL,
 `restriction_criteria` longtext,
 PRIMARY KEY (`pid`),
 UNIQUE KEY `project_number` (`project_number`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `timetrack_time_entry` (
 `teid` int(11) NOT NULL AUTO_INCREMENT,
 `uid` int(11) NOT NULL,
 `year` int(11) NOT NULL,
 `week_number` int(11) NOT NULL,
 `pid` int(11) NOT NULL,
 `mon` int(11) DEFAULT NULL,
 `tue` int(11) DEFAULT NULL,
 `wed` int(11) DEFAULT NULL,
 `thu` int(11) DEFAULT NULL,
 `fri` int(11) DEFAULT NULL,
 `sat` int(11) DEFAULT NULL,
 `sun` int(11) DEFAULT NULL,
 `total` int(11) DEFAULT NULL,
 `comments` varchar(255) DEFAULT NULL,
 `created` int(11) NOT NULL,
 `submitted` int(11) DEFAULT NULL,
 PRIMARY KEY (`teid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
