<?php

namespace Drupal\timetrack\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\Condition;
use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a route controller for watches autocomplete form elements.
 */
class ProjectsController extends ControllerBase {

  /**
  * The database connection.
  *
  * @var \Drupal\Core\Database\Connection
  */
 protected $database;

 /**
  * @param \Drupal\Core\Database\Connection $database
  *  The database connection.
  */
 public function __construct(Connection $database) {
     $this->database = $database;
   }

 /**
  * {@inheritdoc}
  */
 public static function create(ContainerInterface $container) {
   return new static(
     $container->get('database')
   );
  }

  /**
   * Handler for autocomplete request.
   */
  public function listProjects(Request $request) {
    $projects = [];
    $projectsQuery = $request->query->get('q');

    // Get the typed string from the URL, if it exists.
    if (!$projectsQuery) {
      return new JsonResponse($projects);
    }

    $projectsQuery = Xss::filter($projectsQuery);

    $query = $this->database->select('timetrack_projects', 'p');
    $query->fields('p', ['pid', 'project_number', 'project_name']);
    // projects matching name or number with query param.
    $projectNumberOrName = $query->orConditionGroup()
      ->condition('project_number',"%" . $this->database->escapeLike($projectsQuery) . "%", 'LIKE')
      ->condition('project_name', "%" . $this->database->escapeLike($projectsQuery) . "%", 'LIKE');
    $query->condition($projectNumberOrName);
    $query->condition('project_status', 'Active');
    $results = $query->execute()->fetchAll();
    foreach($results as $key => $result){
      $projects[] = [
        'value' => $result->project_number,
        'label' => $result->project_name,
      ];
    }

    return new JsonResponse($projects);
  }
}
