<?php

namespace Drupal\timetrack\Services;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\Condition;
use Drupal\timetrack\Services\ProjectService;

/**
 * Defines a service for Time Entry.
 */
class TimeEntryService {

  /**
  * The database connection.
  *
  * @var \Drupal\Core\Database\Connection
  */
 protected $database;

 /**
 * The database connection.
 *
 * @var \Drupal\timetrack\Services\ProjectService
 */
protected $projectService;

 /**
  * @param \Drupal\Core\Database\Connection $database
  *  The database connection.
  * @param \Drupal\timetrack\Services\ProjectService
  * The project service
  */
 public function __construct(Connection $database, ProjectService $ps) {
   $this->database = $database;
   $this->projectService = $ps;
 }

  /**
   * Add time entry in databse.
   *
   * @param int $uid
   *   User id.
   * @param int $year
   *   Time entry year.
   * @param int $week
   *   Time entry week.
   * @param array $timeData
   *   Time entry data.
   * @param int $submitted
   *   Time entry data submitted.
   */
  public function addTimeEntry(int $uid, int $year, int $week, array $timeData, int $submitted = NULL): void {
    $query = $this->database->insert('timetrack_time_entry')->fields([
      'uid',
      'pid',
      'year',
      'week_number',
      'mon',
      'tue',
      'wed',
      'thu',
      'fri',
      'sat',
      'sun',
      'total',
      'comments',
      'created',
      'submitted'
    ]);
    foreach($timeData as $time){
      if(isset($time) && isset($time['pid'])){
        $record = [
          $uid,
          $this->projectService->getProjectId($time['pid']),
          $year,
          $week,
          !empty($time['mon']) ? $time['mon'] : 0,
          !empty($time['tue']) ? $time['tue'] : 0,
          !empty($time['wed']) ? $time['wed'] : 0,
          !empty($time['thu']) ? $time['thu'] : 0,
          !empty($time['fri']) ? $time['fri'] : 0,
          !empty($time['sat']) ? $time['sat'] : 0,
          !empty($time['sun']) ? $time['sun'] : 0,
          !empty($time['total']) ? $time['total'] : 0,
          $time['comments'] ?? '',
          \Drupal::time()->getRequestTime(),
          $submitted,
        ];
        if($time['teid']){
          $this->updateTimeEntry($time['teid'], $record);
        }
        else{
          $query->values($record);
        }
      }
    }
    $query->execute();
  }

  /**
   * Update time entry in databse.
   *
   * @param int $teid
   *   Time entry id.
   * @param int $uid
   *   User id.
   * @param int $year
   *   Time entry year.
   * @param int $week
   *   Time entry week.
   * @param array $timeData
   *   Time entry data.
   */
  public function updateTimeEntry(int $teid, array $timeData): void {
    $this->database->update('timetrack_time_entry')
      ->fields([
        'uid' => $timeData[0],
        'pid' => $timeData[1],
        'year' => $timeData[2],
        'week_number' => $timeData[3],
        'mon' => $timeData[4],
        'tue' => $timeData[5],
        'wed' => $timeData[6],
        'thu' => $timeData[7],
        'fri' => $timeData[8],
        'sat' => $timeData[9],
        'sun' => $timeData[10],
        'total' => $timeData[11],
        'comments' => $timeData[12],
        'created' => $timeData[13],
        'submitted' => $timeData[14],
      ])
      ->condition('teid', $teid)
      ->execute();
  }

  /**
   * Delete time entry in databse.
   *
   * @param array $timeData
   *   Time entry data.
   */
  public function deleteTimeEntry(array $timeData): void {

  }

  /**
   * Get time entry in databse.
   *
   * @param int $uid
   *   User id.
   * @param int $year
   *   Time entry year.
   * @param int $week
   *   Time entry week.
   * @return array $timeEntry
   *   Time entry data.
   */
  public function getTimeEntry(int $uid, int $year, int $week): array {
    $query = $this->database->select('timetrack_time_entry', 't');
    $query->fields('t', [
      'teid',
      'pid',
      'mon',
      'tue',
      'wed',
      'thu',
      'fri',
      'sat',
      'sun',
      'total',
      'comments'
    ]);
    $query->condition('t.uid', $uid);
    $query->condition('t.year', $year);
    $query->condition('t.week_number', $week);
    $results = $query->execute()->fetchAll();
    $timeEntry = [];
    foreach($results as $key => $result){
      $timeEntry[$key]['teid'] = $result->teid;
      $timeEntry[$key]['pid'] = $this->projectService->getProjectNumber($result->pid);
      $timeEntry[$key]['mon'] = $result->mon;
      $timeEntry[$key]['tue'] = $result->tue;
      $timeEntry[$key]['wed'] = $result->wed;
      $timeEntry[$key]['thu'] = $result->thu;
      $timeEntry[$key]['fri'] = $result->fri;
      $timeEntry[$key]['sat'] = $result->sat;
      $timeEntry[$key]['sun'] = $result->sun;
      $timeEntry[$key]['total'] = $result->total;
      $timeEntry[$key]['comments'] = $result->comments;
    }
    return $timeEntry;
  }

}
