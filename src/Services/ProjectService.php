<?php

namespace Drupal\timetrack\Services;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\Condition;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\taxonomy\TermStorageInterface;

/**
 * Defines a service for Time Entry.
 */
class ProjectService {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
  * The database connection.
  *
  * @var \Drupal\Core\Database\Connection
  */
 protected $database;

 /**
  * @param \Drupal\Core\Database\Connection $database
  *  The database connection.
  */
 public function __construct(Connection $database) {
   $this->database = $database;
 }

  /**
   * Add time entry in databse.
   *
   * @param int $uid
   *   User id.
   * @param array $projectData
   *   Project data.
   */
  public function addProject(int $uid, array $projectData): void {
    $query = $this->database->insert('timetrack_projects')->fields([
      'project_number',
      'project_name',
      'project_department',
      'project_type',
      'project_status',
      'restricted',
      'restriction_criteria',
      'created_by',
      'created',
    ]);
    if($projectData['project_number']){
      $record = [
        !empty($projectData['project_number']) ? $projectData['project_number'] : NULL,
        !empty($projectData['project_name']) ? $projectData['project_name'] : NULL,
        !empty($projectData['project_department']) ? $projectData['project_department'] : NULL,
        !empty($projectData['project_type']) ? $projectData['project_type'] : NULL,
        !empty($projectData['project_status']) ? $projectData['project_status'] : NULL,
        !empty($projectData['restricted']) ? $projectData['restricted'] : NULL,
        !empty($projectData['restriction_criteria']) ? $projectData['restriction_criteria'] : NULL,
        $uid,
        \Drupal::time()->getRequestTime(),
      ];
      if($projectData['pid']){
        $this->updateProject($projectData['pid'], $record);
      }
      else{
        $query->values($record);
        $query->execute();
        $this->messenger()->addStatus($this->t('Project created successfuly.'));
      }
    }
  }

  /**
   * Update time entry in databse.
   *
   * @param int $pid
   *   Project id.
   * @param array $projectData
   *   Project data.
   */
  public function updateProject(int $pid, array $projectData): void {
    $this->database->update('timetrack_projects')
      ->fields([
        'project_number' => $projectData[0],
        'project_name' => $projectData[1],
        'project_department' => $projectData[2],
        'project_type' => $projectData[3],
        'project_status' => $projectData[4],
        'restricted' => $projectData[5],
        'restriction_criteria' => $projectData[6],
      ])
      ->condition('pid', $pid)
      ->execute();
    if($projectData[4] != 'Active'){
      $this->deleteFavoriteProjectById($pid);
    }
    $this->messenger()->addStatus($this->t('Project updated successfuly.'));
  }

  /**
   * Get time entry in databse.
   *
   * @param string $project_number
   *   Project id.
   * @return int $pid
   *   Project entry data.
   */
  public function getProjectId(string $project_number): ?int {
    if($project_number){
      $query = $this->database->select('timetrack_projects', 'p');
      $query->fields('p', [
        'pid',
      ]);
      $query->condition('p.project_number', $project_number);
      $result = $query->execute()->fetchAssoc('pid');
    }
    return $result['pid'] ?? NULL;
  }

  /**
   * Get time entry in databse.
   *
   * @param int $pid
   *   Project id.
   * @return string $project_number
   *   Project number.
   */
  public function getProjectNumber(int $pid): string {
    $query = $this->database->select('timetrack_projects', 'p');
    $query->fields('p', [
      'project_number',
    ]);
    $query->condition('p.pid', $pid);
    $result = $query->execute()->fetchAssoc('project_number');
    return $result['project_number'];
  }

  /**
   * Get projects in databse.
   *
   * @param int $pid
   *   Project id.
   * @return array $project
   *   Project entry data.
   */
  public function getProjectInformation(int $pid = NULL): array {
    $projectInformation = [];
    if($pid){
      $query = $this->database->select('timetrack_projects', 'p');
      $query->fields('p', [
        'project_number',
        'project_name',
        'project_department',
        'project_type',
        'project_status',
        'restricted',
        'restriction_criteria',
      ]);
      $query->condition('p.pid', $pid);
      $results = $query->execute()->fetchAll();
      foreach($results as $key => $result){
        $projectInformation[$key]['project_number'] = $result->project_number;
        $projectInformation[$key]['project_name'] = $result->project_name;
        $projectInformation[$key]['project_department'] = $result->project_department;
        $projectInformation[$key]['peoject_type'] = $result->project_type;
        $projectInformation[$key]['peoject_status'] = $result->project_status;
        $projectInformation[$key]['restricted'] = $result->restricted;
        $projectInformation[$key]['restriction_criteria'] = $result->restriction_criteria;
      }
    }
    return $projectInformation;
  }

  /**
   * Get projects in databse.
   *
   * @param int $uid
   *   Project id.
   * @return array $favoriteproject
   *   Project entry data.
   */
  public function getFavoriteProjects(int $uid): array {
    $favoriteproject = [];
    if($uid){
      $query = $this->database->select('timetrack_projects_favorite', 'p');
      $query->fields('p', [
        'pid',
      ]);
      $query->condition('p.uid', $uid);
      $results = $query->execute()->fetchAll();
      foreach($results as $key => $result){
        $favoriteproject[$key]['pid'] = $this->getProjectNumber($result->pid);
      }
    }
    return $favoriteproject;
  }

  /**
   * Add time entry in databse.
   *
   * @param int $uid
   *   User id.
   * @param array $favoriteProjectData
   *   Favorite project data.
   */
  public function addFavoriteProject(int $uid, array $favoriteProjectData): void {
    $query = $this->database->insert('timetrack_projects_favorite')->fields([
      'uid',
      'pid',
      'created',
    ]);
    foreach($favoriteProjectData as $fp){
      if($fp['pid']){
        $record = [
          $uid,
          $this->getProjectId($fp['pid']),
          \Drupal::time()->getRequestTime(),
        ];
        $query->values($record);
      }
    }
    $query->execute();
    $this->messenger()->addStatus($this->t('Project(s) successfuly added to your favorite list.'));
  }

  /**
   * Delete favorite project in databse.
   *
   * @param int $uid
   *   User id.
   * @param int $fpid
   *   Favorite project id.
   */
  public function deleteFavoriteProject(int $uid, int $fpid): void {
    $this->database->delete('timetrack_projects_favorite')
      ->condition('uid', $uid)
      ->condition('fpid', $fpid)
      ->execute();
    $this->messenger()->addStatus($this->t('Successfuly removed the project from your favorite list.'));
  }

  /**
   * Delete favorite project in databse.
   *
   * @param int $pid
   *   Project id.
   */
  public function deleteFavoriteProjectById(int $pid): void {
    $this->database->delete('timetrack_projects_favorite')
      ->condition('pid', $pid)
      ->execute();
  }

  /**
   * Retrieves the term storage.
   *
   * @return \Drupal\taxonomy\TermStorageInterface
   *   The term storage.
   */
  public function getTermStorage() {
    return $this->termStorage ?: \Drupal::service('entity_type.manager')
      ->getStorage('taxonomy_term');
  }

}
