<?php

namespace Drupal\timetrack\Form;

use Drupal\Core\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Term;

/**
 * Example ajax add remove buttons.
 *
 * This example demonstrates using ajax callbacks to add people's names
 * to a list of picnic attendees with an option to remove specific people.
 */
class FavoriteProjectForm extends FormBase {

  /**
   * Required by FormBase.
   */
  public function getFormId() {
    return 'form_project_favorite_entry';
  }

  /**
   * Form with 'add more' and 'remove' buttons.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $year = \Drupal::routeMatch()->getRawParameter('uid');
    $week = \Drupal::routeMatch()->getRawParameter('fpid');
    $form['#tree'] = TRUE;
    $form['favorite_project'] = [
      '#type' => 'fieldset',
      '#prefix' => '<div id="favorite-project-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];
    // Get the number of records available in table.
    $num_lines = $form_state->get('num_lines');
    // We have to ensure that there is at least one name field.
    if ($num_lines === NULL) {
      $form_state->set('num_lines', 1);
      $num_lines = $form_state->get('num_lines');
    }
    // Get a list of fields that were removed.
    $removed_fields = $form_state->get('removed_fields');
    // If no fields have been removed yet we use an empty array.
    if ($removed_fields === NULL) {
      $form_state->set('removed_fields', []);
      $removed_fields = $form_state->get('removed_fields');
    }

    for ($i = 0; $i < $num_lines; $i++) {
      // Check if field was removed.
      if (in_array($i, $removed_fields)) {
        // Skip if field was removed and move to the next field.
        continue;
      }
      $form['favorite_project'][$i] = [
        '#type' => 'fieldset',
        '#attributes' => [
          'class' => ['favorite-project-entry', 'time-track-entry'],
        ],
      ];
      $form['favorite_project'][$i]['pid'] = [
        '#type' => 'textfield',
        '#title' => 'Favorite Project',
        '#autocomplete_route_name' => 'timetrack.autocomplete.projects',
        '#default_value' => '',
        '#placeholder' => $this->t('Favorite Project'),
        '#attributes' => [
          'class' => ['favorite-project-project', 'time-track-project'],
        ],
      ];
      $form['favorite_project'][$i]['actions'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove'),
        '#name' => $i,
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => 'favorite-project-fieldset-wrapper',
        ],
      ];
    }

    $form['favorite_project']['actions'] = [
      '#type' => 'actions',
    ];

    $form['favorite_project']['actions']['add_name'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add one more'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'favorite-project-fieldset-wrapper',
      ],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    $form['#attributes']['class'] = 'time-track-favorite-project-form';
    $form['#attached']['library'][] = 'timetrack/timetrack.time_entry';

    return $form;

  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['favorite_project'];
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $num_field = $form_state->get('num_lines');
    $add_button = $num_field + 1;
    $form_state->set('num_lines', $add_button);
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove" button.
   *
   * Removes the corresponding line.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    /*
     * We use the name of the remove button to find
     * the element we want to remove
     * Line 72: '#name' => $i,.
     */
    $trigger = $form_state->getTriggeringElement();
    $indexToRemove = $trigger['#favorite_project'];

    // Remove the fieldset from $form (the easy way)
    unset($form['favorite_project'][$indexToRemove]);

    // Remove the fieldset from $form_state (the hard way)
    // First fetch the fieldset, then edit it, then set it again
    // Form API does not allow us to directly edit the field.
    $namesFieldset = $form_state->getValue('favorite_project');
    unset($namesFieldset[$indexToRemove]);
    // $form_state->unsetValue('names_fieldset');
    $form_state->setValue('favorite_project', $namesFieldset);

    // Keep track of removed fields so we can add new fields at the bottom
    // Without this they would be added where a value was removed.
    $removed_fields = $form_state->get('removed_fields');
    $removed_fields[] = $indexToRemove;
    $form_state->set('removed_fields', $removed_fields);

    // Rebuild form_state.
    $form_state->setRebuild();
  }

  /**
   * Required by FormBase.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $favorite_project = $form_state->cleanValues()->getValues()['favorite_project'];
    /*foreach($favorite_project as $key => $fp) {
      if(empty($time['favorite_project'])) {
        $form_state->setError($form['favorite_project'][$key]['pid'], $this->t('Please enter valid project'));
      }
    }*/
    parent::validateForm($form, $form_state);
  }

  /**
   * Required by FormBase.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $uid = \Drupal::currentUser()->id();
    $favoriteProject = $form_state->cleanValues()->getValues()['favorite_project'];
    \Drupal::service('timetrack.project')->addFavoriteProject($uid, $favoriteProject);
  }

}
