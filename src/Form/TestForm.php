<?php

namespace Drupal\timetrack\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\Condition;
use Drupal\Core\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\timetrack\Services\TimeEntryService;

/**
 * Time entry form.
 */
class TestForm extends FormBase {

  /**
   * Required by FormBase.
   */
  public function getFormId() {
    return 'form_time_tracking';
  }

  /**
   * Form with 'add more' and 'remove' buttons.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $uid = \Drupal::currentUser()->id();

    $form['#tree'] = TRUE;
    $form['timeentry'] = [
      '#type' => 'fieldset',
      '#prefix' => '<div id="names-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];
    $year = \Drupal::routeMatch()->getRawParameter('year') ?? date("Y");
    $week = \Drupal::routeMatch()->getRawParameter('week_number') ?? date("W");
    // Call get timeEntry service
    //$timeEntry = \Drupal::service('timetrack.timeentry')->getTimeEntry($uid, $year, $week);
    $timeEntry = [];
    // Get the number of records available in table.
    $num_lines = $form_state->get('num_lines');
    // We have to ensure that there is at least one name field.
    if ($num_lines === NULL) {
      $form_state->set('num_lines', 1);
      $num_lines = $form_state->get('num_lines');
    }
    // Get a list of fields that were removed.
    $removed_fields = $form_state->get('removed_fields');
    // If no fields have been removed yet we use an empty array.
    if ($removed_fields === NULL) {
      $form_state->set('removed_fields', []);
      $removed_fields = $form_state->get('removed_fields');
    }

    for ($i = 0; $i < $num_lines; $i++) {
      // Check if field was removed.
      if (in_array($i, $removed_fields)) {
        // Skip if field was removed and move to the next field.
        continue;
      }
      $form['timeentry'][$i] = [
        '#type' => 'fieldset',
        '#attributes' => [
          'class' => ['time-track-entry'],
        ],
      ];
      $form['timeentry'][$i]['teid'] = [
        '#type' => 'hidden',
        '#default_value' => $timeEntry[$i]['teid'] ?? '',
      ];
      $form['timeentry'][$i]['pid'] = [
        '#type' => 'textfield',
        '#title' => 'Project',
        '#autocomplete_route_name' => 'timetrack.autocomplete.projects',
        '#default_value' => $timeEntry[$i]['pid'] ?? '',
        '#placeholder' => $this->t('Project'),
        '#attributes' => [
          'class' => ['time-track-project'],
        ],
      ];
      $form['timeentry'][$i]['mon'] = [
        '#type' => 'textfield',
        '#title' => 'Mon',
        '#default_value' => $timeEntry[$i]['mon'] ?? '',
        '#placeholder' => $this->t('0'),
        '#attributes' => [
          'class' => ['time-track-hours'],
        ],
      ];
      $form['timeentry'][$i]['tue'] = [
        '#type' => 'textfield',
        '#title' => 'Tue',
        '#default_value' => $timeEntry[$i]['tue'] ?? '',
        '#placeholder' => $this->t('0'),
        '#attributes' => [
          'class' => ['time-track-hours'],
        ],
      ];
      $form['timeentry'][$i]['wed'] = [
        '#type' => 'textfield',
        '#title' => 'Wed',
        '#default_value' => $timeEntry[$i]['wed'] ?? '',
        '#placeholder' => $this->t('0'),
        '#attributes' => [
          'class' => ['time-track-hours'],
        ],
      ];
      $form['timeentry'][$i]['thu'] = [
        '#type' => 'textfield',
        '#title' => 'Thu',
        '#default_value' => $timeEntry[$i]['thu'] ?? '',
        '#placeholder' => $this->t('0'),
        '#attributes' => [
          'class' => ['time-track-hours'],
        ],
      ];
      $form['timeentry'][$i]['fri'] = [
        '#type' => 'textfield',
        '#title' => 'Fri',
        '#default_value' => $timeEntry[$i]['fri'] ?? '',
        '#placeholder' => $this->t('0'),
        '#attributes' => [
          'class' => ['time-track-hours'],
        ],
      ];
      $form['timeentry'][$i]['sat'] = [
        '#type' => 'textfield',
        '#title' => 'Sat',
        '#default_value' => $timeEntry[$i]['sat'] ?? '',
        '#placeholder' => $this->t('0'),
        '#attributes' => [
          'class' => ['time-track-hours'],
        ],
      ];
      $form['timeentry'][$i]['sun'] = [
        '#type' => 'textfield',
        '#title' => 'Sun',
        '#default_value' => $timeEntry[$i]['sun'] ?? '',
        '#placeholder' => $this->t('0'),
        '#attributes' => [
          'class' => ['time-track-hours'],
        ],
      ];
      $form['timeentry'][$i]['total'] = [
        '#type' => 'textfield',
        '#title' => 'Total',
        '#default_value' => $timeEntry[$i]['total'] ?? '',
        '#placeholder' => $this->t('0'),
        '#attributes' => [
          'class' => ['time-track-hours time-track-total'],
        ],
      ];
      $form['timeentry'][$i]['comments'] = [
        '#type' => 'textfield',
        '#title' => 'Comments ',
        '#default_value' => $timeEntry[$i]['comments'] ?? '',
        '#placeholder' => $this->t('0'),
        '#attributes' => [
          'class' => ['time-track-comments'],
        ],
      ];
      $form['timeentry'][$i]['actions'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove'),
        '#name' => $i,
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => 'names-fieldset-wrapper',
        ],
      ];
    }

    $form['timeentry']['actions'] = [
      '#type' => 'actions',
    ];

    $form['timeentry']['actions']['add_name'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add one more'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'names-fieldset-wrapper',
      ],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    $form['#attributes']['class'] = 'time-track-form';
    $form['#attached']['library'][] = 'timetrack/timetrack.time_entry';

    return $form;

  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['timeentry'];
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $num_field = $form_state->get('num_lines');
    $add_button = $num_field + 1;
    $form_state->set('num_lines', $add_button);
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove" button.
   *
   * Removes the corresponding line.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    /*
     * We use the name of the remove button to find
     * the element we want to remove
     * Line 72: '#name' => $i,.
     */
    $trigger = $form_state->getTriggeringElement();
    $indexToRemove = $trigger['#name'];

    // Remove the fieldset from $form (the easy way)
    unset($form['timeentry'][$indexToRemove]);

    // Remove the fieldset from $form_state (the hard way)
    // First fetch the fieldset, then edit it, then set it again
    // Form API does not allow us to directly edit the field.
    $namesFieldset = $form_state->getValue('names_fieldset');
    unset($namesFieldset[$indexToRemove]);
    // $form_state->unsetValue('names_fieldset');
    $form_state->setValue('names_fieldset', $namesFieldset);

    // Keep track of removed fields so we can add new fields at the bottom
    // Without this they would be added where a value was removed.
    $removed_fields = $form_state->get('removed_fields');
    $removed_fields[] = $indexToRemove;
    $form_state->set('removed_fields', $removed_fields);

    // Rebuild form_state.
    $form_state->setRebuild();
  }

  /**
   * Required by FormBase.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $timeEntry = $form_state->cleanValues()->getValues()['timeentry'];
    /*foreach($timeEntry as $key => $time) {
      if(empty($time['project'])) {
        $form_state->setError($form['timeentry'][$key]['project'], $this->t('Please enter valid project'));
      }
    }*/
    parent::validateForm($form, $form_state);
  }

  /**
   * Required by FormBase.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $uid = \Drupal::currentUser()->id();
    $timeEntry = $form_state->cleanValues()->getValues()['timeentry'];
    $submitted = NULL;
    \Drupal::service('timetrack.timeentry')->addTimeEntry($timeEntry, $submitted);
  }

}
