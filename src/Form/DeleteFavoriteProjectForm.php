<?php

namespace Drupal\timetrack\Form;

use Drupal\Core\Ajax\AjaxResponse;
Use Drupal\Core\Ajax\CloseModalDialogCommand;
USE Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Term;

/**
 * Example ajax add remove buttons.
 *
 * This example demonstrates using ajax callbacks to add people's names
 * to a list of picnic attendees with an option to remove specific people.
 */
class DeleteFavoriteProjectForm extends FormBase {

  /**
   * Required by FormBase.
   */
  public function getFormId() {
    return 'form_project_favorite_delete';
  }

  /**
   * Form with 'add more' and 'remove' buttons.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $uid = \Drupal::routeMatch()->getRawParameter('uid');
    $fpid = \Drupal::routeMatch()->getRawParameter('fpid');
    if($uid && $fpid){
      $form['help'] = [
        '#type' => 'markup',
        '#markup' => '<div>You are about to remove project from your favorite list.<br>Are you sure?</div>',
      ];
    }
    else{
      $form['help'] = [
        '#type' => 'markup',
        '#markup' => '<div>You are not authorized to access this page.</div>',
      ];
    }
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Yes, Delete'),
    ];
    $form['actions']['cancel'] = [
      '#type' => 'submit',
      '#value' => $this->t('cancel'),
      '#ajax' => [
        'callback' => '::closeModalForm',
      ],
    ];

    $form['#attributes']['class'] = 'time-track-favorite-project-form';
    $form['#attached']['library'][] = 'timetrack/timetrack.time_entry';

    return $form;

  }

  /**
   * Required by FormBase.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Required by FormBase.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $uid = \Drupal::routeMatch()->getRawParameter('uid');
    $fpid = \Drupal::routeMatch()->getRawParameter('fpid');
    \Drupal::service('timetrack.project')->deleteFavoriteProject($uid, $fpid);
    $form_state->setRedirect('timetrack.project_favirite_add');
    return;
  }

  /**
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function closeModalForm() {
    $command = new CloseModalDialogCommand();
    $response = new AjaxResponse();
    $response->addCommand($command);
    return $response;
  }

}
