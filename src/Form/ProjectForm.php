<?php

namespace Drupal\timetrack\Form;

use Drupal\Core\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Term;

/**
 * Example ajax add remove buttons.
 *
 * This example demonstrates using ajax callbacks to add people's names
 * to a list of picnic attendees with an option to remove specific people.
 */
class ProjectForm extends FormBase {

  /**
   * Required by FormBase.
   */
  public function getFormId() {
    return 'form_project_entry';
  }

  /**
   * Form with 'add more' and 'remove' buttons.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // call project service to get information
    $pid = \Drupal::routeMatch()->getRawParameter('pid');
    $project = \Drupal::service('timetrack.project')->getProjectInformation($pid);
    $form['pid'] = [
      '#type' => 'hidden',
      '#default_value' => $pid,
    ];
    $form['project_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Project Number'),
      '#placeholder' => $this->t('Project Number'),
      '#default_value' => $project[0]['project_number'] ?? '',
      '#attributes' => [
        'class' => ['time-track-project-number'],
      ],
      '#required' => TRUE,
    ];
    $form['project_name'] = [
      '#title' => $this->t('Project Name'),
      '#type' => 'textfield',
      '#placeholder' => $this->t('Project Name'),
      '#default_value' => $project[0]['project_name'] ?? '',
      '#attributes' => [
        'class' => ['time-track-project-name'],
      ],
      '#required' => TRUE,
    ];
    $projectDepartment = $projectType = [];
    $terms =\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('time_track_department');
    foreach ($terms as $term) {
      $projectDepartment[$term->tid] = $term->name;
    }
    $form['project_department'] = [
      '#title' => $this->t('Project Department'),
      '#type' => 'select',
      '#options' => $projectDepartment,
      '#default_value' => $project[0]['project_department'] ?? 0,
      '#attributes' => [
        'class' => ['time-track-project-department'],
      ],
      '#required' => TRUE,
    ];
    $terms =\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('time_track_project_types');
    foreach ($terms as $term) {
      $projectType[$term->tid] = $term->name;
    }
    $form['project_type'] = [
      '#title' => $this->t('Project Type'),
      '#type' => 'select',
      '#options' => $projectType,
      '#default_value' => $project[0]['peoject_type'] ?? '',
      '#attributes' => [
        'class' => ['time-track-project-type'],
      ],
      '#required' => TRUE,
    ];
    $form['project_status'] = [
      '#title' => $this->t('Project Status'),
      '#type' => 'select',
      '#options' => [
        'Active' => 'Active',
        'Inactive' => 'Inactive',
        'Closed' => 'Closed'
      ],
      '#default_value' => $project[0]['peoject_status'] ?? '',
      '#attributes' => [
        'class' => ['time-track-project-status'],
      ],
      '#required' => TRUE,
    ];
    $form['restricted'] = [
      "#title" => $this->t('Is project restricted?'),
      '#type' => 'select',
      '#options' => [
        "No" => 'No',
        "Yes" => 'Yes',
      ],
      '#default_value' => $project[0]['restricted'] ?? '',
      '#attributes' => [
        'class' => ['time-track-hours'],
      ],
    ];
    $form['restriction_criteria'] = [
      "#title" => $this->t('Restriction criteria'),
      '#type' => 'textfield',
      '#placeholder' => $this->t('Provide restriction criteria'),
      '#default_value' => $project[0]['restriction_criteria'] ?? '',
      '#attributes' => [
        'class' => ['time-track-hours'],
      ],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    $form['#attributes']['class'] = 'time-track-project-form';
    $form['#attached']['library'][] = 'timetrack/timetrack.project';

    return $form;

  }

  /**
   * Required by FormBase.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if(empty($form_state->getValue('project_number'))) {
      $form_state->setError($form['project_number'], $this->t('Please provide Project Number'));
    }
    if(empty($form_state->getValue('project_name'))) {
      $form_state->setError($form['project_name'], $this->t('Please provide Project Name'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * Required by FormBase.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $uid = \Drupal::currentUser()->id();
    $projectEntry = $form_state->cleanValues()->getValues();
    \Drupal::service('timetrack.project')->addProject($uid, $projectEntry);
  }

}
